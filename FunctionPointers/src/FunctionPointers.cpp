//============================================================================
// Name        : FunctionPointers.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>

// Pointers to Function
// ////////////////////////////////////////////////////////////////////////////

double add(double left, double right) {
    return left + right;
}

double multiply(double left, double right) {
    return left * right;
}

double binary_op(double left, double right, double (*f)(double, double)) {
    return (*f)(left, right);
}

// Functors
// ////////////////////////////////////////////////////////////////////////////

class BinaryFunction
{
public:
  virtual ~BinaryFunction() = default;
  virtual double operator() (double left, double right) = 0;
};


class Add : public BinaryFunction {
public:
  Add() {};
  virtual ~Add() = default;
  virtual double operator() (double left, double right) { return left+right; }
};


class Multiply : public BinaryFunction {
public:
  Multiply() {};
  virtual ~Multiply() = default;
  virtual double operator() (double left, double right) { return left*right; }
};

double binary_op(double left, double right, BinaryFunction* bin_func) {
  return (*bin_func)(left, right);
}

int main( )
{
    double a = 5.0;
    double b = 10.0;

    std::cout << "Add: " << binary_op(a, b, add) << std::endl;
    std::cout << "Multiply: " << binary_op(a, b, multiply) << std::endl;

    std::cout << " + + + + + + + + + + + + + + + + " << std::endl;

    // ///////////////////////////////////////////////////////////////////////////////////////////

    BinaryFunction* pAdd = new Add();
    BinaryFunction* pMultiply = new Multiply();

    std::cout << "Add: " << binary_op(a, b, pAdd) << std::endl;
    std::cout << "Multiply: " << binary_op(a, b, pMultiply) << std::endl;

    delete pAdd;
    delete pMultiply;

    return 0;
}
