//============================================================================
// Name        : TimerChrono.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <cstdint>
#include "headers/TimerClass.h"

void myFunction(uint8_t aVal)
{
	TimerClass tc;
	for (uint8_t index = 0; index < aVal; ++index)
	{
		std::cout << "Printing message ...\n";
	}
}

int main()
{
	myFunction(50);
	return 0;
}
