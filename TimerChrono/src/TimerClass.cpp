/*
 * TimerClass.cpp
 *
 *  Created on: Apr 26, 2019
 *      Author: tpawlik
 */

#include "headers/TimerClass.h"

TimerClass::TimerClass() {
	mStart = std::chrono::system_clock::now();
}

TimerClass::~TimerClass() {
	mEnd = std::chrono::system_clock::now();
	auto time = mEnd - mStart;


    std::cout << "Completed in: "
              << std::chrono::duration_cast<std::chrono::microseconds>(time).count()
              << "us.\n";

}

std::chrono::system_clock::time_point TimerClass::getStart()
{
	return mStart;
}

std::chrono::system_clock::time_point TimerClass::getEnd()
{
	return mEnd;
}
