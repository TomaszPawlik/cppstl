/*
 * TimerClass.h
 *
 *  Created on: Apr 26, 2019
 *      Author: tpawlik
 */

#ifndef TIMERCLASS_H_
#define TIMERCLASS_H_

#include <chrono>
#include <iostream>

class TimerClass {
public:
	TimerClass();
	virtual ~TimerClass();

	std::chrono::system_clock::time_point getStart();
	std::chrono::system_clock::time_point getEnd();
private:
	std::chrono::system_clock::time_point mStart;
	std::chrono::system_clock::time_point mEnd;
};

#endif /* TIMERCLASS_H_ */
