/*
 * MyContainer.h
 *
 *  Created on: Apr 9, 2019
 *      Author: tpawlik
 */



#ifndef MYCONTAINER_H_
#define MYCONTAINER_H_

#include <array>
#include <vector>
#include <list>
#include <deque>
#include<forward_list>
#include <stack>
#include <queue>

#include <iterator>
#include <map>
#include <set>

class MyContainer
{
public:
	// Sequence Containers: implement data structures which can be accessed in a sequential manner.
	std::array<int,4> arrayVar {};
	std::vector<int> vectorVar {1,2,3,4};
	std::list<int> listVar {4,3,2,1};
	std::deque<int> dequeVar {5,6,7,8};
	std::forward_list<int> flistVar {1,2,3};

	// Container Adaptors : provide a different interface for sequential containers.
	std::stack<int> stackVar;
    std::priority_queue<int> pqueueVar;
    std::queue<int> queueVar;

    // Associative Containers:  implement sorted data structures that can be quickly searched (O(log n) complexity).
    std::map<int, int> mapVar;	// Klucz, wartość
    std::set<int> setVar {3,2,1}; // Zbior unikatowych wartosci
    std::multiset<int> multisetsetVar {3,2,1,3,2,1}; // Jak Set - ale wartosci moga sie powtarzac
    std::multimap<int, int> multimapVar;
};

#endif /* MYCONTAINER_H_ */
