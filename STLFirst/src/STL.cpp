#include <iostream>
#include "headers/MyContainer.h"

#include <algorithm>
#include <utility> // std::pair

// Przeniesc funkcje ponizej do klasy
// Zamiast na int operowac na obiektach
// Rozica miedzy emplace a push


void display(const int& aVar)
{
	std::cout << aVar << " ";
}

void inc(int& aVar)
{
	++aVar;
}

bool over5(const int& aVar)
{
	return aVar > 5;
}

void showstack(std::stack <int> s)
{
    while (!s.empty())
    {
        std::cout << '\t' << s.top();
        s.pop();
    }
    std::cout << '\n';
}

void showpq(std::priority_queue <int> gq)
{
    while (!gq.empty())
    {
    	std::cout << '\t' << gq.top();
    	gq.pop();
    }
    std::cout << '\n';
}

void showq(std::queue <int> gq)
{
    while (!gq.empty())
    {
    	std::cout << '\t' << gq.front();
    	gq.pop();
    }
    std::cout << '\n';
}

void showmap(std::map<int,int> aMap)
{
    std::map<int, int>::iterator itr;
    std::cout << "KEY\tELEMENT\n";
    for (itr = aMap.begin(); itr != aMap.end(); ++itr)
    {
    	std::cout << itr->first << '\t' << itr->second << '\n';
    }
    std::cout << std::endl;
}

int main() {

	MyContainer tc;		// Test container

	std::cout << "Size of array is: " << tc.arrayVar.size() << std::endl;

	tc.arrayVar.at(0) = 1;
	tc.arrayVar[1] = 2;
	tc.arrayVar.front() = 3;
	*(tc.arrayVar.begin()+3) = 10;

	std::for_each(tc.arrayVar.begin(),tc.arrayVar.end(),display);
	std::cout << std::endl;
	std::cout << "++++++++++++++++++++++++++++++++++++++++++++" << std::endl;

	std::cout << "Capacity of vector is: " << tc.vectorVar.capacity() << std::endl;

	// W wektorze dodajemy jedynie na koniec
	tc.vectorVar.push_back(5);					// Push_back nie tworzy nowego elementu
	tc.vectorVar.emplace_back(6);				// Nowy element + dodanie na koniec
	tc.vectorVar.erase(tc.vectorVar.end()-1);

	for (auto it = tc.vectorVar.begin(); it != tc.vectorVar.end(); ++it)
		++(*it);
	std::for_each(tc.vectorVar.begin(),tc.vectorVar.end(),inc);

	std::for_each(tc.vectorVar.begin(),tc.vectorVar.end(),display);
	std::cout << std::endl;

	std::cout << "Capacity of vector is: " << tc.vectorVar.capacity() << std::endl;

	std::cout << "++++++++++++++++++++++++++++++++++++++++++++" << std::endl;

	tc.listVar.sort();
	tc.listVar.reverse();
	tc.listVar.pop_front();
	tc.listVar.pop_back();

	auto it = tc.listVar.begin();
	std::advance(it,1);							//Przesuniecie iteratora it na [1]
	tc.listVar.insert(it,1,5);					//Miejsce, ilosc elementow, wartosc
	tc.listVar.emplace(begin(tc.listVar),6);
	tc.listVar.remove_if(over5);

	std::cout << "List: " << std::endl;
	std::for_each(tc.listVar.begin(),tc.listVar.end(),display);
	std::cout << std::endl;

	std::cout << "++++++++++++++++++++++++++++++++++++++++++++" << std::endl;
	// Deque - jak wektor ale mozna dodawac tez na poczatek. Nie ma gwarancji ciaglosci.

	tc.dequeVar.push_back(10);
	tc.dequeVar.push_front(20);
	tc.dequeVar.push_back(30);
	tc.dequeVar.push_front(15);
	tc.dequeVar.at(3) = 0;

	std::cout << "Deque: " << std::endl;
    std::for_each(tc.dequeVar.begin(),tc.dequeVar.end(),display);
	std::cout << std::endl;

	std::cout << "++++++++++++++++++++++++++++++++++++++++++++" << std::endl;
	// Forward_list - infromacja tylko o nastepnym elemencie. Mniej miejsca jak list ale przechodzenie tylko w przod.

	tc.flistVar.push_front(4);
	tc.flistVar.insert_after(tc.flistVar.begin(), {5, 6, 7});

	std::cout << "Fowrard list: " << std::endl;
    std::for_each(tc.flistVar.begin(),tc.flistVar.end(),display);
	std::cout << std::endl;

	std::cout << "++++++++++++++++++++++++++++++++++++++++++++" << std::endl;
	// Stack

	std::cout << "Stack: \t";
	tc.stackVar.push(1);
	tc.stackVar.push(2);
	tc.stackVar.push(4);
	tc.stackVar.pop();
	showstack(tc.stackVar);

	std::cout << "++++++++++++++++++++++++++++++++++++++++++++" << std::endl;
	// Priority Queue - najwieksze elementy sa ustawiane na top

	std::cout << "P Queue: ";
	tc.pqueueVar.push(1);
	tc.pqueueVar.push(5);
	tc.pqueueVar.push(3);
	tc.pqueueVar.push(4);
	tc.pqueueVar.push(2);
	showpq(tc.pqueueVar);

	std::cout << "++++++++++++++++++++++++++++++++++++++++++++" << std::endl;
	// Queue - FIFO - szybciej przychodzi, szybciej wychodzi

	std::cout << "Queue: \t";
	tc.queueVar.push(1);
	tc.queueVar.push(5);
	tc.queueVar.push(3);
	tc.queueVar.push(4);
	tc.queueVar.push(2);
	tc.queueVar.pop();
	showq(tc.queueVar);

	std::cout << "++++++++++++++++++++++++++++++++++++++++++++" << std::endl;
	// Map - klucz + wartosc. Klucze sa sortowane

	tc.mapVar.insert(std::pair<int, int>(5, 50));
	tc.mapVar.insert(std::pair<int, int>(2, 20));
	tc.mapVar.insert(std::pair<int, int>(4, 40));
	tc.mapVar.insert(std::pair<int, int>(3, 30));
	tc.mapVar.insert(std::pair<int, int>(4, 20));	// Element nie zostanie dodany bo jest juz element o kluczu 4
	showmap(tc.mapVar);

	std::map<int, int>::iterator iteratorMap = tc.mapVar.begin();
	iteratorMap++;
	std::cout << "Iterator: " << (iteratorMap)->second << std::endl;

	tc.mapVar[2] = 22;
	tc.mapVar.at(5) = 55;
	tc.mapVar.erase(3);

	std::cout << tc.mapVar[4] << std::endl;						// w nawiasie podajemy klucz
	std::cout << tc.mapVar.at(5) << std::endl;					// rowniez podajemy klucz
	std::cout << (tc.mapVar.begin())->second << std::endl;		// second - wartosc

	std::cout << "++++++++++++++++++++++++++++++++++++++++++++" << std::endl;
	// Set - zbiór unikalnych wartosci, automatycznie sortowany

	tc.setVar.insert(4);
	tc.setVar.erase(2);
	std::for_each(tc.setVar.begin(),tc.setVar.end(),display);
	std::cout << std::endl;

	std::cout << "++++++++++++++++++++++++++++++++++++++++++++" << std::endl;
	// MultiSet

	auto counter = tc.multisetsetVar.count(2);
	tc.multisetsetVar.erase(2);		// Wyrzuca WSZYSTKIE elementy o wartosic 2
	for (int i = 0; i < (counter -1); ++i)	// W ten sposob mozna zrobic wyrzucenie tylko jednego elementu
		tc.multisetsetVar.insert(2);

	std::for_each(tc.multisetsetVar.begin(),tc.multisetsetVar.end(),display);
	std::cout << std::endl;

	std::cout << "++++++++++++++++++++++++++++++++++++++++++++" << std::endl;
	// MultiMap



	return 0;
}
