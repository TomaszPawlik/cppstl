/*
 * MyTestClass.cpp
 *
 *  Created on: Apr 25, 2019
 *      Author: tpawlik
 */

#include "headers/MyTestClass.h"

MyTestClass::MyTestClass(int aInt, uint8_t aUInt) : intValue(aInt), uIntValue(aUInt)
{
	std::cout << "Const: " << this << std::endl;
}

MyTestClass::~MyTestClass()
{
	std::cout << "Dest: " << this << std::endl;
}

int MyTestClass::getInt() 					{return intValue;}
uint8_t MyTestClass::getUInt() 				{return uIntValue;}
void MyTestClass::setInt(int aVal)			{intValue = aVal;}
void MyTestClass::setUInt(uint8_t aVal) 	{uIntValue = aVal;}

void MyTestClass::sayHello()				{std::cout << "Hello from " << this << std::endl;}
