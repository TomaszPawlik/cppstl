/*
 * MyTestClass.h
 *
 *  Created on: Apr 25, 2019
 *      Author: tpawlik
 */

#ifndef MYTESTCLASS_H_
#define MYTESTCLASS_H_

#include <cstdint>			// uint8_t
#include <iostream>

class MyTestClass
{
public:
	MyTestClass() = delete;
	MyTestClass(int aInt, uint8_t aUInt);
	~MyTestClass();

	int getInt();
	uint8_t getUInt();
	void setInt(int aVal);
	void setUInt(uint8_t aVal);

	void sayHello();

private:
	int intValue;
	uint8_t uIntValue;
};

#endif /* MYTESTCLASS_H_ */
