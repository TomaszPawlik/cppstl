//============================================================================
// Name        : SmartPointers.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <cstdint>			// uint8_t
#include <memory>			// unique_ptr
#include <vector>
#include "headers/MyTestClass.h"


// Example implemenation of "smart pointer"
// //////////////////////////////////////////////////////////////

// A generic smart pointer class
template <class T>
class SmartPtr
{
   T *ptr;  // Actual pointer
public:
   // Constructor
   explicit SmartPtr(T *p = NULL) { ptr = p; }		// explicit - Specifies that a constructor or conversion function (since C++11) is explicit, that is, it cannot be used for implicit conversions and copy-initialization.

   // Destructor
   ~SmartPtr() { delete(ptr); }

   // Overloading dereferncing operator
   T & operator * () {  return *ptr; }

   // Overloding arrow operator so that members of T can be accessed
   // like a pointer (useful if T represents a class or struct or
   // union type)
   T * operator -> () { return ptr; }
};

// Difference between raw and smart pointers

void bugged_func()
{
    int* valuePtr = new int(15);
    int x = 45;
    // ...
    if (x == 45)
        return;   // here we have a memory leak, valuePtr is not deleted
    // ...
    delete valuePtr;
}


void fixed_func()
{
    std::unique_ptr<int> valuePtr(new int(15));
    int x = 45;
    // ...
    if (x == 45)
        return;   // no memory leak anymore!
    // ...
}


int main()
{
	uint8_t sadsadsad = 11;

    SmartPtr<int> ptr(new int());
    *ptr = sadsadsad;
    std::cout << "Self implemenation of \"Smart Pointer\": " << *ptr << std::endl;

	// unique_ptr - move available. copy not available
	// ////////////////////////////////////////////////////////////

	std::unique_ptr<int> uPtr1(new int(47));
	std::unique_ptr<std::string>  strPtr(new std::string);
	// std::unique_ptr<int> uPtr2 = std::make_unique<int>();	// Since C++ 14

	*uPtr1 = 30;
	*strPtr = "Hi!";
	strPtr->assign("Hello world");

	//std::cout << strPtr << std::endl;				// Error !!!
	std::cout << uPtr1.get() << std::endl;			//Ptr address
	std::cout << strPtr.get() << std::endl;			//Ptr address
	std::cout << *uPtr1 << std::endl;				//Ptr dereference - int
	std::cout << *uPtr1.get() << std::endl;				//Ptr dereference - int
	std::cout << *strPtr << std::endl;				//Ptr dereference - string
	std::cout << *strPtr.get() << std::endl;		//Ptr dereference + get value

	std::cout << sizeof(uPtr1) << std::endl;



	std::cout << " +++ Unq Pointer on MyClass +++ " << std::endl;



	std::vector<std::unique_ptr<MyTestClass>> unqMTCvec;
	unqMTCvec.reserve(3);

	std::unique_ptr<MyTestClass> mtcPtr(new MyTestClass(10, 20));
	int a = mtcPtr->getInt(); // int a = (*mtcPtr).getInt();
	mtcPtr->sayHello();
	mtcPtr = nullptr;

	if (!mtcPtr)
	{
		std::cout << "Pointer is null" << std::endl;
	}

	//unqMTCvec.push_back(mtcPtr);
	unqMTCvec.emplace_back(new MyTestClass(20,30));
	unqMTCvec.emplace_back(new MyTestClass(30,40));

	for (auto& idx : unqMTCvec)
	{
		idx->sayHello();
		std::cout << idx->getInt() << std::endl;
	}

	std::cout << "Size of vector: " << unqMTCvec.size() << std::endl;

	// shared_ptr - pointer która ma dodatkowy licznik referencji
	// weak_ptr
	// auto_ptr - przestarzały rodzaj pointera. Od CPP11 zastepiony przez unique_pointer

	return 0;
}
