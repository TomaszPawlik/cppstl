//============================================================================
// Name        : STLLambda.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <vector>
#include <map>
#include <algorithm>
#include <random>
#include <utility> // std::pair

int main()
{

	// [ capture_list ]( parameters ){ body_of_function } -> opcjonalny typ zwracany
	// zmienne poza Lambda sa dla niej niewidoczne - dlatego jest capture list
	// jesli chcemy zmieniac wartosci pol w capture list to musimy je przekazac jako referencje
	// = w capture list - przekazanie wszystkich wartosci przez wartosc
	// & w capture list - przekazanie wszystkich wartosci przez referencje
	// oczywiscie mozna przekazac np. jeden parametr przez referencje a inne przez wartosc

    std::random_device dev;
    std::mt19937 rng(dev());
    std::uniform_int_distribution<std::mt19937::result_type> dist(0,10); // distribution in range [1, 10]

	int i = 10;
	int g = 11;

	auto addLambda = [=, &g](int a, int b) -> int
			{
				g = 15;
				return a + b + i + g;
			};

	std::cout << "Add lambda: " << addLambda(1, 2) << std::endl;

	// Vector
	// ///////////////////////////////////////////////////////////////////////////////////////////////////////

	std::cout << " + + + + + + + + +  Vector + + + + + + + + + \n";

	int sumOfVector = 0;
	int counterOdd = 0;
	int counterEven = 0;
	int sumOdd = 0;
	int placeOfFirstElementOver99 = 0;
	constexpr int numbOfRandEleInVec = 10;

	std::vector<int> helpVector {100, 200, 300, 400, 500};

    for (int oneHundredInts = 0; oneHundredInts < numbOfRandEleInVec; ++oneHundredInts)
    	helpVector.emplace_back(dist(rng));


	std::sort(helpVector.begin(),helpVector.end());

	std::for_each(helpVector.begin(),helpVector.end(), [&sumOfVector](int& x)
			{
				//x += 1;
				sumOfVector += x;
			});

	counterOdd = std::count_if(helpVector.begin(),helpVector.end(), [](int x) -> bool
			{
				return ((x % 2) == 1);
			});
	counterEven = helpVector.size() - counterOdd;

	sumOdd = std::accumulate(helpVector.begin(), helpVector.end(),0, [](int totalsum, int x) -> int
			{
				if ((x % 2) == 1)
					totalsum += x;
				return totalsum;
			});

	auto itPlace = std::find_if(helpVector.begin(), helpVector.end(), [](int x) -> bool
			{
				return x > 99;
			});
	placeOfFirstElementOver99 = itPlace - helpVector.begin();

	for (auto i : helpVector)
		std::cout << i << " ";
	std::cout << "\n";

	std::cout << "Sum of vector: " << sumOfVector << std::endl;
	std::cout << "Size of vector: " << helpVector.size() << std::endl;
	std::cout << "Odd elements cnt: " << counterOdd << std::endl;
	std::cout << "Even elements cnt: " << counterEven << std::endl;
	std::cout << "Odd elements sum: " << sumOdd << std::endl;
	std::cout << "Index of first element over 99: " << placeOfFirstElementOver99 << std::endl;

	// Map
	// ///////////////////////////////////////////////////////////////////////////////////////////////////////

	std::cout << " + + + + + + + + +  M A P + + + + + + + + + \n";

	std::map<char, int> smallChars;
	for (int index = 97; index < 123; ++index)
		smallChars.insert(std::pair<char, int>(static_cast<char>(index), index));

	std::cout << "Number of elements in map : " << smallChars.size() << "\n";

	// Działa +++
	for (auto index : smallChars)
		std::cout << "[" << index.first << "] = " << index.second << '\n';
    std::cout << std::endl;

/*
	// Działa +++
    for (auto itr = smallChars.begin(); itr != smallChars.end(); ++itr)
    {
		std::cout << "[" << itr->first << "] = " << itr->second << '\n';
    }
    std::cout << std::endl;

	// Działa +++
    std::for_each(smallChars.begin(), smallChars.end(), [](std::pair<char, int> mapPair)
    		{
				std::cout << "[" << mapPair.first << "] = " << mapPair.second << '\n';
    		});
*/

	// Here are some additional changes in Lambda file on master branch.

	return 0;
}
